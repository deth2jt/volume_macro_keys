//https://www.prusaprinters.org/prints/30745-vms-macro-keyboard
//https://arduino.stackexchange.com/questions/13545/using-progmem-to-store-array-of-structs
//https://www.iot-experiments.com/webusb-arduino-pro-micro-atmega32u4/
//https://www.codeproject.com/Articles/1116175/Using-Ardunio-Flash-Memory-for-variable-parameters
#include <ClickEncoder.h>
#include <TimerOne.h>
#include <HID-Project.h>
#include <LiquidCrystal.h>
#include <avr/pgmspace.h>
#include <PROGMEM_readAnything.h>

#define ENCODER_CLK A0 // Change A0 to, for example, A5 if you want to use analog pin 5 instead
#define ENCODER_DT A1
#define ENCODER_SW 10

#define NUMBER_OF_KEYS         8    // Count of keys in the keyboard
#define MAX_COMBINATION_KEYS   4    // Maximum number of key codes that can be pressed at the same time (does dont correspond to actually pressed keys)
#define MAX_SEQUENCE_KEYS     16    // Maximum length of key combination sequence (that means first you send CTRL + Z (1. combination), then SHIFT + ALT + X (2. combination), then A (3. combination) ... )

#define DEBOUNCING_MS         20    // wait in ms when key can oscilate
#define FIRST_REPEAT_CODE_MS 500    // after FIRST_REPEAT_CODE_MS ,s if key is still pressed, start sending the command again
#define REPEAT_CODE_MS       150    // when sending command by holding down key, wait this long before sending command egain
#define NUMOFOPTS           2
#define NUMOFANALOGKEYS      4
#define NUMBER_OF_FUN_KEYS     7

//const char sketchName[] PROGMEM = "oversizeknobV1";  

const static int  rs   = 9, en = 8, d4 = 7, d5 = 6, d6 = 5, d7 = 4;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
int currentOpts;

//int pins[NUMBER_OF_KEYS] = {2, 3, 16, 14, A3};

ClickEncoder *encoder; // variable representing the rotary encoder
int16_t last, value; // variables for current and last rotation value
uint8_t globalModifier;    // when holding down key with MODIFIER type, this is set to true - can be used to change the behaviour of other keys or rotary encoder

// Defining types
enum TKeyState {INACTIVE, DEBOUNCING, ACTIVE, HOLDING};         // Key states - INACTIVE -> DEBOUNCING -> ACTIVE -> HOLDING -> INACTIVE
//                                                                                                               -> INACTIVE
enum TKeyType {KEYBOARD, MOUSE, CONSUMER, SYSTEM, MODIFIER};    // Types of key codes - simulating keyboard, mouse, multimedia or modifier that alters the rotary encoder behavior

typedef struct  TActions {
  //char *desc;
  uint16_t durationMs;
  uint16_t key[MAX_COMBINATION_KEYS];
} TAction;

typedef struct TKeys {
  uint8_t pin;
  //enum TKeyType type;
  enum TKeyState state;
  uint32_t stateStartMs;
  uint16_t modificatorKeys[MAX_COMBINATION_KEYS];
  //TAction action[NUMOFANALOGKEYS][MAX_SEQUENCE_KEYS];
} TKey;

typedef struct  TActionLists {
  char *desc;
  enum TKeyType type;
  TAction action[MAX_SEQUENCE_KEYS];
  //uint16_t key[MAX_COMBINATION_KEYS];
} TActionList;



TKey keyList[NUMBER_OF_KEYS] =
{
  
  {.pin =  2,  .state = INACTIVE, .stateStartMs = 0, .modificatorKeys = {}},
  {.pin =  3,  .state = INACTIVE, .stateStartMs = 0, .modificatorKeys = {} },
  {.pin =  16,  .state = INACTIVE, .stateStartMs = 0, .modificatorKeys = {}},
  {.pin =  14,  .state = INACTIVE, .stateStartMs = 0, .modificatorKeys = {}},
  {.pin =  21,  .state = INACTIVE, .stateStartMs = 0, .modificatorKeys = {}},
  {.pin =  20,  .state = INACTIVE, .stateStartMs = 0, .modificatorKeys = {}},
  {.pin =  15,  .state = INACTIVE, .stateStartMs = 0, .modificatorKeys = {}},
  {.pin =  0,  .state = INACTIVE, .stateStartMs = 0, .modificatorKeys = {}}


};

const static TActionList actionsList1[NUMBER_OF_FUN_KEYS] PROGMEM  =
{
  //{
  {.desc = "P01" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_H}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc =  "P02" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_E}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc =  "P03" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_L}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc =  "P04" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_O}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc =  "P05" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_F}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc =  "P06" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_O}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc =  "P07" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_I}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}}
  //{.desc =  "OPT8" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_H}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}}
  //}
};

const static TActionList actionsList2[NUMBER_OF_FUN_KEYS] PROGMEM =
{
  //{
  {.desc =  "P09" ,  .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_Q}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}  }  },
  {.desc =  "P10" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_A}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc =  "P11" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_Z}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc =  "P12" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_W}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc =  "P13" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_S}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc = "P14" ,  .type = KEYBOARD, .action =  {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_X}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}},
  {.desc = "P15" ,  .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_H}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}}
  //{.desc = "OPT16" , .type = KEYBOARD, .action = {{.durationMs = 50, .key = {KEY_LEFT_SHIFT, KEY_H}}, {.durationMs = 50, .key = {KEY_E}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_L}}, {.durationMs = 50, .key = {KEY_O}}}}
  //}
};

const static TActionList* const actionsL[NUMOFOPTS] PROGMEM = {actionsList1, actionsList2};

void timerIsr() {
  encoder->service();
}

// number of items in an array
//template< typename T, size_t N > size_t ArraySize (T (&) [N]){ return N; }

// Execute key commands
uint8_t processKey(int keyIndex, TActionList (&option)[NUMBER_OF_FUN_KEYS]) {

  ////lcd.setCursor(0, 1);
  //lcd.print(keyIndex);

	//int index = (int) keyIndex;
	if(keyIndex == NUMBER_OF_FUN_KEYS)
	{
		currentOpts = (currentOpts + 1) % 2;
		//currentOpts = 1;
		//return;
		//int c = 0;
		//lcd.setCursor(15, 1);
		//lcd.print(currentOpts);
	}
	else
	{
  
		  TActionList lkey = option[keyIndex];

		  if (lkey.type == KEYBOARD) {
			// Press modificators
			/*
			  for (uint8_t i = 0; i < MAX_COMBINATION_KEYS; i++) {
				if (lkey->modificatorKeys[i]) Keyboard.press((KeyboardKeycode) lkey->modificatorKeys[i]);
				else break;
			  }
			*/
			//lcd.setCursor(5, 1);
			//lcd.print("?");

			for (uint8_t i = 0; i < MAX_SEQUENCE_KEYS; i++) {
			  TAction laction = lkey.action[i];

			  //lcd.setCursor(3, 1);
			  //lcd.print(laction.durationMs);
			  //lcd.setCursor(10, 1);
			  //lcd.print(laction.key[0]);


			  if ((laction.durationMs) || (laction.key[0]))
			  {
				//press keys
				//lcd.setCursor(13, 1);
				//lcd.print("me");

				for (uint8_t j = 0; j < MAX_COMBINATION_KEYS; j++)
				{
				  if (laction.key[j]) Keyboard.press((KeyboardKeycode) laction.key[j]);
				  else break;
				}
				// wait
				if (laction.durationMs) delay(laction.durationMs);
				//release keys
				for (uint8_t j = 0; j < MAX_COMBINATION_KEYS; j++)
				{
				  if (laction.key[j]) Keyboard.release((KeyboardKeycode) laction.key[j]);
				  else break;
				}
			  }
			  else
			  {
				break;
			  }
			}
			Keyboard.releaseAll();
		  } else if (lkey.type == CONSUMER) {
			for (uint8_t i = 0; i < MAX_SEQUENCE_KEYS; i++) {
			  TAction laction = lkey.action[i];
			  if ((laction.durationMs) || (laction.key[0])) {
				//press keys
				for (uint8_t j = 0; j < MAX_COMBINATION_KEYS; j++) {
				  if (laction.key[j]) Consumer.press((ConsumerKeycode) laction.key[j]);
				  else break;
				}
				// wait
				if (laction.durationMs) delay(laction.durationMs);
				//release keys
				for (uint8_t j = 0; j < MAX_COMBINATION_KEYS; j++) {
				  if (laction.key[j]) Consumer.release((ConsumerKeycode) laction.key[j]);
				  else break;
				}
			  } else {
				break;
			  }
			}
			Consumer.releaseAll();
		  } else if (lkey.type == SYSTEM) {
			if (lkey.action[0].key[0]) {
			  System.write((SystemKeycode) lkey.action[0].key[0]);
			}
		  } else if (lkey.type == MOUSE) {
			for (uint8_t i = 0; i < MAX_SEQUENCE_KEYS; i++) {
			  // MOUSE - no modifiers,
			  // if durationMs >= 10000; then in key is tripplet- delta movement in X, Y and SCROLL (zero is mapped to 10000, so 9800 is -200px) - {{.durationMs = 10200, .key = {10000, 10000, 10300}} - scroll 300px and wait 200ms
			  // if durationMs < 10000; then in key are mouse keys to press  - {{.durationMs = 150, .key = {MOUSE_LEFT, MOUSE_MIDDLE}} - press left and middle mouse buttons for 150ms
			  TAction laction = lkey.action[i];
			  if ((laction.durationMs) || (laction.key[0])) {
				if (laction.durationMs < 10000) { // button's clicks
				  //press keys
				  for (uint8_t j = 0; j < MAX_COMBINATION_KEYS; j++) {
					if (laction.key[j]) Mouse.press((uint8_t) laction.key[j]);
					else break;
				  }
				  // wait
				  if (laction.durationMs) delay(laction.durationMs);
				  //release keys
				  for (uint8_t j = 0; j < MAX_COMBINATION_KEYS; j++) {
					if (laction.key[j]) Mouse.release((uint8_t) laction.key[j]);
					else break;
				  }
				} else {  // cursor movement and scrolling
				  int8_t x = laction.key[0] - 10000;
				  int8_t y = laction.key[1] - 10000;
				  int8_t wheel = laction.key[2] - 10000;
				  Mouse.move(x, y, wheel);
				  if (laction.durationMs) delay(laction.durationMs - 10000);
				}
			  } else {
				break;
			  }
			}
			Mouse.releaseAll();
		  } else if (lkey.type == MODIFIER) {
			globalModifier = true;
		  } else {
			//Serial.println(F("Error - unsupported keyboard type"));
			;
		  }
	}

}

//TODO -defiuntely
short funfunfunction(short intastic)
{
	if(currentOpts == 0)
		return 0;
	else if(currentOpts == 0)
		return 1;
}

void setup()
{
  Serial.begin(9600); // Opens the serial connection used for communication with the PC.

  Consumer.begin(); // Initializes the media keyboard
  Keyboard.begin();
  Mouse.begin();
  System.begin();

  
  currentOpts = 0;
  encoder = new ClickEncoder(ENCODER_DT, ENCODER_CLK, ENCODER_SW); // Initializes the rotary encoder with the mentioned pins

  Timer1.initialize(1000); // Initializes the timer, which the rotary encoder uses to detect rotation
  Timer1.attachInterrupt(timerIsr);
  last = -1;

  //TODO
  //TKey key[NUMBER_OF_KEYS];
  //TKey *key = keyList[index];

  for (uint8_t i = 0; i < NUMBER_OF_KEYS; i++)
    pinMode(keyList[i].pin, INPUT_PULLUP);
  globalModifier = false;

  lcd.backlight();
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  //lcd.print("hello, worldfo1u");
  //lcd.setCursor(0, 0);
  //lcd.print("Starting up.....");
}

void loop() {
 
	//int currentOpts;
	TActionList option[NUMBER_OF_FUN_KEYS];
	
	//short *index = &currentOpts;
	//if(currentOpts == 0)
	//short index = funfunfunction(currentOpts);
	for (int i = 0; i < NUMBER_OF_FUN_KEYS; i++)
    {
		if(currentOpts == 0)
			PROGMEM_readAnything (&actionsL[0][i], option[i]);
		else if(currentOpts == 1)
			PROGMEM_readAnything (&actionsL[1][i], option[i]);
		
		//TODO in function
		lcd.setCursor( (i % 4) *4 ,i / 4);		
		//lcd.print(i);
		//lcd.setCursor( ((i % 4) *4) + 1 ,  i / 4);
		lcd.print(option[i].desc);
		//PROGMEM_readAnything (&actionsL[1][i], option[i]);
	}
	lcd.setCursor(12, 1);
	lcd.print("NEXT");
	
	//lcd.setCursor(10, 1);	
	//lcd.print(option[6].desc);
	
  
  for (uint8_t i = 0; i < NUMBER_OF_KEYS; i++) {


    //uint8_t index = i;
    uint8_t keyState = digitalRead(keyList[i].pin);
    /*
      if(i == NUMBER_OF_ACT_KEYS - 1)
      {
      uint8_t keyState = analogRead(keyList[i].pin);
      uint8_t index = 4 + getButtonNum1(keyState);

      if(index <= 7 && index >= 4)
        keyState = HIGH;
      else
        keyState = LOW;
      }
    */

    if ((keyList[i].state == INACTIVE) && (keyState == LOW))
    {
      keyList[i].state = DEBOUNCING;
      keyList[i].stateStartMs = millis();
    }
    else if (keyList[i].state == DEBOUNCING)
    {
      if (keyState == HIGH) keyList[i].stateStartMs = millis();
      else if ((millis() - keyList[i].stateStartMs) > DEBOUNCING_MS)
      {

        //lcd.setCursor(0, 0);
        //lcd.print("111111 up....");

        keyList[i].state = ACTIVE;
        processKey(i,option);
        //lcd.setCursor(0, 0);
        //lcd.print("111111 11....");
      }
    }
    else if (keyList[i].state == ACTIVE)
    {
      if (keyState == HIGH)
      {
        keyList[i].state = INACTIVE;
        keyList[i].stateStartMs = millis();
        //if (keyList[i].type == MODIFIER)
        if ( option[i].type ==  MODIFIER)
        {
          globalModifier = false;
        }
      }
      else if ((millis() - keyList[i].stateStartMs) > FIRST_REPEAT_CODE_MS)
      {
        //lcd.setCursor(0, 0);
        //lcd.print("22222 up....");


        keyList[i].state = HOLDING;
        keyList[i].stateStartMs = millis();
        processKey(i, option);
        //lcd.setCursor(0, 0);
        //lcd.print("22222 22....");
      }
    }
    else if (keyList[i].state == HOLDING)
    {
      if (keyState == HIGH)
      {
        keyList[i].state = INACTIVE;
        keyList[i].stateStartMs = millis();


        if (option[i].type ==  MODIFIER)
        {
          globalModifier = false;
        }

      }
      else if ((millis() - keyList[i].stateStartMs) > REPEAT_CODE_MS)
      {
        //lcd.setCursor(0, 0);
        //lcd.print("33333 up.....");



        keyList[i].stateStartMs = millis();
        processKey(i, option);
        //lcd.setCursor(0, 0);
        //lcd.print("33333 33.....");
      }
    }
  }


  // ROTARY_ACTIONS
  // Turning the rotary encoder
  value += encoder->getValue();

  // This part of the code is responsible for the actions when you rotate the encoder
  if (value != last) {                          // New value is different than the last one, that means to encoder was rotated
    uint16_t diff = abs(value - last);
    if (globalModifier) {                       // The rotary encoder is used as volume control or with the Modifier key as a scroll
      signed char wheel = (last < value) ? 4 : -4;
      for (uint8_t i = 0; i < diff; i++) Mouse.move(0, 0, wheel);
    } else {
      ConsumerKeycode cmd = (last > value) ? MEDIA_VOLUME_UP : MEDIA_VOLUME_DOWN;
      for (uint8_t i = 0; i < diff; i++) Consumer.write(cmd);
    }
    last = value;                                 // Refreshing the "last" varible for the next loop with the current value
  }

  // Pressing the rotary encoder - detects single and double clicks
  // This next part handles the rotary encoder BUTTON
  ClickEncoder::Button b = encoder->getButton(); // Asking the button for it's current state
  if (b != ClickEncoder::Open) {                 // If the button is unpressed, we'll skip to the end of this if block
    switch (b) {
      case ClickEncoder::Clicked:                // Button was clicked once
        Consumer.write(MEDIA_VOLUME_MUTE);       // Replace this line to have a different function when double-clicking
        break;
      case ClickEncoder::DoubleClicked:          // Button was double clicked
        Consumer.write(MEDIA_PLAY_PAUSE);        // Replace this line to have a different function when clicking button once

        break;
    }
  }
  delay(5);                                    // I think this is not needed
}

